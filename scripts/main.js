// jmantic2 minteri2
console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("name-text").value;
    console.log('Name:  ' + name);
    makeNetworkCallToApi(name);

} // end of get form info

function makeNetworkCallToApi(name){
    console.log('entered makeNetworkCallToApi');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.ipify.org/?format=json";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateIpWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateIpWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);

    var label1 = document.getElementById("response-line1");

    if(response_json['ip'] == null){
        label1.innerHTML = 'Sorry ' + name + ', we could not find an ip address for you.'
    } else{
        label1.innerHTML =  name + ', your ip address is: ' + response_json['ip'];
        var ip = response_json['ip'];
        console.log(ip);
        makeNetworkCallToIPApi(ip);
    }
}

function makeNetworkCallToIPApi(ip){
    console.log('entered make nw call' + ip);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.ip2country.info/ip?" + ip;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateCountryWithResponse(ip, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateCountryWithResponse(ip, response_text){
    var response_json = JSON.parse(response_text);

    var label2 = document.getElementById("response-line2");

    if(response_json['countryName'] == null){
        label2.innerHTML = 'Apologies, we could not find a country for you.'
    } else{
        label2.innerHTML =  'The country for your IP Adress (' + ip + ') is ' + response_json['countryName'];
        var country = response_json['countryName'];
        console.log(country);
    }
}
